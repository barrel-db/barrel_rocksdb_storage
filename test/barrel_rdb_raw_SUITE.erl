%%%-------------------------------------------------------------------
%%% @author benoitc
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 05. Apr 2018 17:40
%%%-------------------------------------------------------------------
-module(barrel_rdb_raw_SUITE).
-author("benoitc").

-author("benoitc").

%% API
-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/2
]).

-export([
  store_basic/1,
  barrel_basic/1
]).

all() ->
  [
    store_basic,
    barrel_basic
  ].

init_per_suite(Config) ->
  {ok, _} = application:ensure_all_started(barrel_rocksdb_storage),
  Config.

init_per_testcase(_, Config) ->
  Config.

end_per_testcase(_, _Config) ->
  ok.

end_per_suite(Config) ->
  ok = application:stop(barrel_rocksdb_storage),
  ok = application:stop(barrel),
  _ = (catch rocksbd:destroy(barrel_rocksdb_storage:store_path(test, #{ path => "/tmp" }))),
  Config.


store_basic(_) ->
  {ok, Store} = barrel_rocksdb_storage:init(test, #{ path => "/tmp" }),
  #{ name := test, path := Path } = Store,
  true = filelib:is_dir(Path),
  ok = barrel_rocksdb_storage:terminate(Store),
  ok = rocksdb:destroy(Path, []),
  false = filelib:is_dir(Path).


barrel_basic(_) ->
  {ok, #{ last_id := 0} = Store} = barrel_rocksdb_storage:init(test, #{ path => "/tmp" }),
  {{ok, Barrel}, #{ last_id := 1 } = Store2} = barrel_rocksdb_storage:create_barrel(<<"testing">>, #{}, Store),
  {{ok, Barrel2}, #{ last_id := 2 } = Store3} = barrel_rocksdb_storage:create_barrel(<<"testing2">>, #{}, Store2),
  {{error, already_exists}, _} = barrel_rocksdb_storage:create_barrel(<<"testing2">>, #{}, Store3),
  {ok, Barrel} = barrel_rocksdb_storage:open_barrel(<<"testing">>, Store3),
  {ok, Barrel2} = barrel_rocksdb_storage:open_barrel(<<"testing2">>, Store3),

  #{ name := <<"testing">>, id := 1, docs_count := 0, updated_seq := 0 } = Barrel,
  #{ name := <<"testing2">>, id := 2, docs_count := 0, updated_seq := 0 } = Barrel2,

  {ok, _} = barrel_rocksdb_storage:delete_barrel(<<"testing">>, Store3),
  {error, not_found} = barrel_rocksdb_storage:open_barrel(<<"testing">>, Store3),
  {{ok, Barrel_1}, #{ last_id := 3 } = Store4} = barrel_rocksdb_storage:create_barrel(<<"testing">>, #{}, Store3),
  {ok, Barrel_1} = barrel_rocksdb_storage:open_barrel(<<"testing">>, Store4),
  true = (Barrel_1 =/= Barrel),
  ok = barrel_rocksdb_storage:destroy(Store4).








