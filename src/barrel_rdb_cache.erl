%%%-------------------------------------------------------------------
%%% @author benoitc
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 04. Apr 2018 22:50
%%%-------------------------------------------------------------------
-module(barrel_rdb_cache).
-author("benoitc").
-behaviour(gen_server).

%% API
-export([
  get_cache/0,
  get_usage/0,
  get_pinned_usage/0,
  get_capacity/0,
  set_capacity/1
]).

-export([
  start_link/0
]).

-export([
  init/1,
  handle_call/3,
  handle_cast/2,
  terminate/2
]).

-type cache() :: term().

%% ---- api

-spec get_cache() -> cache().
get_cache() ->
  gen_server:call(?MODULE, get_cache).

-spec get_usage() -> non_neg_integer().
get_usage() ->
  gen_server:call(?MODULE, get_usage).

-spec get_pinned_usage() -> non_neg_integer().
get_pinned_usage() ->
  gen_server:call(?MODULE, get_pinned_usage).

-spec get_capacity() -> non_neg_integer().
get_capacity() ->
  gen_server:call(?MODULE, get_capacity).

-spec set_capacity(Capacity :: non_neg_integer()) -> ok.
set_capacity(Capacity) ->
  gen_server:call(?MODULE, {set_capacity, Capacity}).


start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% ---- gen_server callbacks

init([]) ->
  process_flag(trap_exit, true),
  Size = cache_size(),
  rocksdb:new_lru_cache(Size).

handle_call(get_cache, _From, Cache) ->
  {reply, Cache, Cache};
handle_call(get_usage, _From, Cache) ->
  {reply, rocksdb:get_usage(Cache), Cache};
handle_call(get_pinned_usage, _From, Cache) ->
  {reply, rocksdb:get_pinned_usage(Cache), Cache};
handle_call(get_capacity, _From, Cache) ->
  {reply, rocksdb:get_capacity(Cache), Cache};
handle_call({set_capacity, Capacity}, _From, Cache) ->
  {reply, rocksdb:set_capacity(Cache, Capacity), Cache};
handle_call(Msg, _From, Cache) ->
  {reply, {bad_call, Msg}, Cache}.

handle_cast(_Msg, State) ->
  {noreply, State}.

terminate(_Reason, Cache) ->
  _ = (catch rocksdb:release_cache(Cache)),
  ok.


%% ---- internals

cache_size() ->
  case application:get_env(barrel_rocksdb_storage, block_cache_size) of
    {ok, Value} ->
      case barrel_resource_monitor_misc:parse_information_unit(Value) of
        {ok, ParsedTotal} ->
          ParsedTotal;
        {error, parse_error} ->
          _ = lager:warning(
            "The override value for the total memmory available is "
            "not a valid value: ~p, getting total from the system.~n",
            [Value]),
          default_cache_size()
      end;
    undefined ->
      default_cache_size()
  end.

default_cache_size() ->
  MaxSize = barrel_vm_memory_monitor:get_total_memory_from_os(),
  %% reserve 1GB for system and binaries, and use 30% of the rest
  trunc((MaxSize - 1024 * 1024) * 0.3).