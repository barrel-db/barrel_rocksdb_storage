%% Copyright (c) 2017. Benoit Chesneau
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%    http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.


%% database metas:
%% databse name <-> database id
%% last database id ?

-module(barrel_rdb_keys).
-author("benoitc").

%% API
-export([
  meta/2,
  doc/2,
  seq/2,
  local/2,
  rev/3,
  barrel_key/1,
  i_fwd/3,
  i_rev/3
]).


-export([
  encode_parts/2
]).

-export([enc/2]).

%% metadata keys

%% db keys

barrel_key(Name) -> << 0, 0, 0, 100, 0, Name/binary >>.

meta(Id, Key) -> << Id:32, 0, 0, 0, Key/binary >>.

doc(Id, DocId) -> << Id:32, 0, 0, 50, 0, DocId/binary >>.

seq(Id, Seq) -> << Id:32, 0, 0, 100, 0, Seq:64 >>.

local(Id, DocId) -> << Id:32, 0, 0, 200, 0, DocId/binary >>.

rev(Id, DocId, RevId) -> << Id:32, DocId/binary, 1, RevId/binary >>.

i_fwd(Id, Path, DocId) ->
  barrel_encoding:encode_binary_ascending(
    encode_parts(Path, << Id:32, 0, 0, 400, 0 >>),
    DocId
  ).

i_rev(Id, Path, DocId) ->
  barrel_encoding:encode_binary_ascending(
    encode_parts(lists:reverse(Path), << Id:32, 0, 0, 410, 0 >>),
    DocId
  ).

%% index keys

encode_parts([P | Rest], Prefix) ->
  encode_parts(Rest, enc(Prefix, P));
encode_parts([], Encoded) ->
  Encoded.

enc(B, P) when is_binary(P) ->
  barrel_encoding:encode_binary_ascending(B, P);
enc(B, P) when is_integer(P) ->
  barrel_encoding:encode_varint_ascending(B, P);
enc(B, P) when is_float(P) ->
  barrel_encoding:encode_float_ascending(B, P);
enc(B, false) ->
  barrel_encoding:encode_literal_ascending(B, false);
enc(B, null) ->
  barrel_encoding:encode_literal_ascending(B, null);
enc(B, true) ->
  barrel_encoding:encode_literal_ascending(B, true);
enc(B, Else) ->
  barrel_encoding:encode_binary_ascending(B, barrel_lib:to_binary(Else)).
