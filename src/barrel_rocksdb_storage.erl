%%%-------------------------------------------------------------------
%%% @author benoitc
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 04. Apr 2018 22:48
%%%-------------------------------------------------------------------
-module(barrel_rocksdb_storage).
-author("benoitc").

%% API
-export([
  init/2,
  terminate/1,
  destroy/1
]).

%% db operations
-export([
  create_barrel/3,
  open_barrel/2,
  delete_barrel/2,
  has_barrel/2,
  close_barrel/2
]).

%% documents
-export([
  get_revision/3,
  add_revision/4,
  delete_revision/3,
  delete_revisions/3,
  fetch_docinfo/2,
  write_docinfo/5,
  purge_doc/4
]).


%% local documents
-export([
  put_local_doc/3,
  get_local_doc/2,
  delete_local_doc/2
]).

-export([
  get_snapshot/1,
  release_snapshot/1
]).

-export([
  save_state/1,
  last_indexed_seq/1
]).

-export([fold_changes/4]).

-export([
  index_path/3,
  unindex_path/3
]).

%% private export useful for the tests
-export([store_path/2]).

%% -- storage behavior

init(Name, Options0) ->
  _ = ensure_started(),
  Path = store_path(Name, Options0),
  Options1 =  [{create_if_missing, true},
              {error_if_exists, true} | db_options(Options0)],
  Retries = application:get_env(barrel_rocksdb_storage, db_open_retries, 30),
  case open_db(Path, Options1, Retries, undefined) of
    {ok, Dbh} ->
      LastId = last_db_id(Dbh),
      {ok, #{name => Name, path => Path, handle => Dbh, last_id => LastId }};
    Error ->
      Error
  end.

terminate(#{ handle := nil }) -> ok;
terminate(#{ handle := H }) -> rocksdb:close(H).

destroy(#{ handle := H, path := Path }) ->
  _ = (catch rocksdb:close(H)),
  rocksdb:destroy(Path, []).

create_barrel(Name, _Options, #{ handle := DbHandle, last_id := LastId0 } = State) ->
  BKey = barrel_rdb_keys:barrel_key(Name),
  case rocksdb:get(DbHandle, BKey, []) of
    {ok, _BarrelId} ->
      {{error, already_exists}, State};
    not_found ->
      LastId1 = LastId0 + 1,
      %% store the db link and increase last_db_id
      Batch = [
        {put, BKey, << LastId1:32  >>},
        {put, << 0, 0, 0, "last_db_id" >>, << LastId1:32  >>}
      ],
      Barrel = #{name => Name,
                 id => LastId1,
                 handle => DbHandle,
                 docs_count => 0,
                 updated_seq => 0,
                 indexed_seq => 0},
      case rocksdb:write(DbHandle, Batch, [{sync, true}]) of
        ok -> {{ok, Barrel}, State#{ last_id => LastId1}};
        Error -> {Error, State}
      end
  end.

open_barrel(Name, #{ handle := DbHandle }) ->
  BKey = barrel_rdb_keys:barrel_key(Name),
  case rocksdb:get(DbHandle, BKey, []) of
    {ok, << BarrelId:32 >>} ->
      Barrel = init_barrel(Name, BarrelId, DbHandle),
      {ok, Barrel};
    not_found ->
      {error, not_found}
  end.

delete_barrel(Name, #{ handle := DbHandle } = State) ->
  BKey = barrel_rdb_keys:barrel_key(Name),
  case rocksdb:get(DbHandle, BKey, []) of
    {ok, << BarrelId:32 >>} ->
      BKey = barrel_rdb_keys:barrel_key(Name),
      case rocksdb:single_delete(DbHandle, BKey, [{sync, true}]) of
        ok ->
          Start = << BarrelId:32 >>,
          End = << (BarrelId +1):32 >>,
          Reply = rocksdb:delete_range(DbHandle, Start, End, []),
          {Reply, State};
        Error ->
          {Error, State}
      end;
    not_found ->
      {ok, State};
    Error ->
      {Error, State}
  end.

has_barrel(Name, #{ handle := DbHandle }) ->
  BKey = barrel_rdb_keys:barrel_key(Name),
  case rocksdb:get(DbHandle, BKey, []) of
    {ok, _} -> true;
    _ -> false
  end.

init_barrel(Name, BarrelId, DbHandle) ->
  DocsCount = docs_count(DbHandle, BarrelId),
  LastSeq = last_sequence(DbHandle, BarrelId),
  IndexedSeq = last_indexed_seq(#{ id => BarrelId, handle => DbHandle }),
  #{name => Name,
    id => BarrelId,
    handle => DbHandle,
    docs_count => DocsCount,
    updated_seq => LastSeq,
    indexed_seq => IndexedSeq}.

close_barrel(_Name, State) ->
  {ok, State}.

save_state(#{ id := Id, handle := Handle, docs_count := Count, indexed_seq := Seq }) ->
  SeqKey = barrel_rdb_keys:meta(Id, <<"indexed_seq">>),
  CountKey = barrel_rdb_keys:meta(Id, <<"docs_count">>),
  Batch = [{put, SeqKey, << Seq:64 >>},
           {put, CountKey, << Count:64 >> }],
  rocksdb:write(Handle, Batch, []).

%% -- docs

get_revision(DocId, RevId, #{ id := Id, handle := Handle } = State) ->
  RevKey = barrel_rdb_keys:rev(Id, DocId, RevId),
  case rocksdb:get(Handle, RevKey, read_options(State)) of
    {ok, Bin} -> {ok, binary_to_term(Bin)};
    not_found -> {error, not_found};
    Error -> Error
  end.

add_revision(DocId, RevId, Body, #{ id := Id, handle := Handle }) ->
  RevKey = barrel_rdb_keys:rev(Id, DocId, RevId),
  rocksdb:put(Handle, RevKey, term_to_binary(Body), []).

delete_revision(DocId, RevId, #{ id := Id, handle := Handle }) ->
  RevKey = barrel_rdb_keys:rev(Id, DocId, RevId),
  rocksdb:delete(Handle, RevKey, []).

delete_revisions(DocId, RevIds, #{ id := Id, handle := Handle }) ->
  Batch = [{delete,  barrel_rdb_keys:rev(Id, DocId, RevId)} || RevId <- RevIds],
  rocksdb:write_batch(Handle, Batch, []).

fetch_docinfo(DocId,  #{ id := Id, handle := Handle } = State) ->
  DIKey = barrel_rdb_keys:doc(Id, DocId),
  case rocksdb:get(Handle, DIKey, read_options(State)) of
    {ok, Bin} -> {ok, binary_to_term(Bin)};
    not_found -> {error, not_found};
    Error -> Error
  end.

write_docinfo(DocId, NewSeq, OldSeq, DocInfo, #{ id := Id, handle := Handle }) ->
  DIKey = barrel_rdb_keys:doc(Id, DocId),
  SeqKey = barrel_rdb_keys:seq(Id, NewSeq),
  DIBin = term_to_binary(DocInfo),
  Batch = case write_action(NewSeq, OldSeq) of
            new ->
              [{put, DIKey, DIBin},
               {put, SeqKey, DIBin}];
            replace ->
              OldSeqKey = barrel_rdb_keys:seq(Id, OldSeq),
              [{put, DIKey, DIBin},
                {put, SeqKey, DIBin},
                {delete, OldSeqKey}];
            edit ->
              [{put, DIKey, DIBin}]
          end,
  rocksdb:write(Handle, Batch, []).


write_action(_Seq, nil) -> new;
write_action(nil, _Seq) -> edit;
write_action(Seq, Seq) -> edit;
write_action(_, _) -> replace.

purge_doc(DocId, LastSeq, Revisions,  #{ id := Id, handle := Handle }) ->
  DIKey = barrel_rdb_keys:doc(Id, DocId),
  SeqKey = barrel_rdb_keys:seq(Id, LastSeq),
  Batch = lists:foldl(
    fun(RevId, Batch1) ->
      [{delete, barrel_rdb_keys:rev(Id, DocId, RevId)} | Batch1]
    end,
    [{delete, SeqKey},
     {delete, DIKey}],
    Revisions
  ),
  rocksdb:write(Handle, Batch, []).

put_local_doc(DocId, Doc, #{ id := Id, handle := Handle }) ->
  LKey = barrel_rdb_keys:local(Id, DocId),
  rocksdb:put(Handle, LKey, term_to_binary(Doc), []).

get_local_doc(DocId,  #{ id := Id, handle := Handle } = State) ->
  LKey = barrel_rdb_keys:local(Id, DocId),
  case rocksdb:get(Handle, LKey, read_options(State)) of
    {ok, Bin} -> {ok, binary_to_term(Bin)};
    not_found -> {error, not_found};
    Error -> Error
  end.

delete_local_doc(DocId, #{ id := Id, handle := Handle }) ->
  LKey = barrel_rdb_keys:local(Id, DocId),
  rocksdb:delete(Handle, LKey, []).

get_snapshot(#{ handle := Handle } = State) ->
  {ok, Snapshot} = rocksdb:snapshot(Handle),
  State#{ snapshot => Snapshot }.


release_snapshot(#{ snapshot := Snapshot }) ->
  rocksdb:release_snapshot(Snapshot);
release_snapshot(_) ->
  ok.

read_options(#{ snapshot := Snapshot }) -> [{snapshot, Snapshot}];
read_options(_) -> [].


fold_changes(Since, Fun, Acc,  #{ id := Id, handle := Handle } = State) ->
  Start = barrel_rdb_keys:seq(Id, Since + 1),
  Prefix = << Id:32, 0, 0, 100 >>,
  {ok, Itr} = rocksdb:iterator(Handle, read_options(State)),
  try fold_changes_loop(rocksdb:iterator_move(Itr, {seek, Start}), Itr, Prefix, Fun, Acc)
  after memstore:iterator_close(Itr)
  end.

fold_changes_loop({ok, << Prefix:7/binary, _Seq:64 >>, DocInfo}, Itr, Prefix, Fun, Acc0) ->
  case Fun(DocInfo, Acc0) of
    {ok, Acc1} ->
      fold_changes_loop(rocksdb:iterator_move(Itr, next), Itr, Prefix, Fun, Acc1);
    {stop, Acc1} ->
      Acc1
  end;
fold_changes_loop(_Else, _, _, _,  Acc) ->
  Acc.


index_path(Path, DocId, #{ id := Id, handle := Handle }) ->
  Batch =  [{put, barrel_rdb_keys:i_fwd(Id, Path, DocId), <<>>},
            {put, barrel_rdb_keys:r_fwd(Id, Path, DocId)}, <<>>],
  rocksdb:write_batch(Handle, Batch, []).

unindex_path(Path, DocId, #{ id := Id, handle := Handle }) ->
  Batch =  [{delete, barrel_rdb_keys:i_fwd(Id, Path, DocId)},
            {delete, barrel_rdb_keys:r_fwd(Id, Path, DocId)}],
  rocksdb:write_batch(Handle, Batch, []).

last_indexed_seq(#{ id := Id, handle := Handle }) ->
  SeqKey = barrel_rdb_keys:meta(Id, <<"indexed_seq">>),
  case rocksdb:get(Handle, SeqKey, []) of
    {ok, << Seq:64 >>} -> Seq;
    not_found -> 0
  end.


%% -- internals

docs_count(DbHandle, BarrelId) ->
  Key = barrel_rdb_keys:meta(BarrelId, <<"docs_count">>),
  case rocksdb:get(DbHandle, Key, []) of
    {ok, << Count:64 >>} -> Count;
    not_found -> 0
  end.

last_sequence(DbHandle, BarrelId) ->
  MaxSeq = barrel_rdb_keys:seq(BarrelId, 1 bsl 64 - 1),
  with_iterator(
    DbHandle, [],
    fun(Itr) ->
      case rocksdb:iterator_move(Itr, {seek_for_prev, MaxSeq}) of
        {ok, << BarrelId:32, 0, 0, 100, Seq:64/integer >>, _} -> Seq;
        _ ->  0
      end
    end).

with_iterator(DbHandle, ReadOptions, Fun) ->
  {ok, Itr} = rocksdb:iterator(DbHandle, ReadOptions),
  try Fun(Itr)
  after rocksdb:iterator_close(Itr)
  end.

last_db_id(DbH) ->
  Key = << 0, 0, 0, "last_db_id" >>,
  case rocksdb:get(DbH, Key, []) of
    {ok, IdBin} -> binary_to_term(IdBin);
    not_found -> 0
  end.

db_dir(Options) ->
  Default = filename:join(barrel_storage:data_dir(), "rdbs"),
  Dir = maps:get(path, Options, Default),
  ok = filelib:ensure_dir(filename:join([Dir, "dummy"])),
  Dir.

db_hash(Name) ->
  barrel_lib:to_list(
    barrel_lib:to_hex(crypto:hash(sha256, barrel_lib:to_binary(Name)))
  ).

store_path(Name, Options) ->
  filename:join([db_dir(Options), db_hash(Name)]).

open_db(_Path, _Options, 0, LastError) ->
  {error, LastError};
open_db(Path, Options, Retries, _LastError) ->
  case rocksdb:open(Path, Options) of
    {ok, _Dbh} = OK -> OK;
    %% Check specifically for lock error, this can be caused if
    %% a crashed instance takes some time to flush leveldb information
    %% out to disk.  The process is gone, but the NIF resource cleanup
    %% may not have completed.
    {error, {db_open, OpenErr}=Reason} ->
      case lists:prefix("IO error: lock ", OpenErr) of
        true ->
          SleepFor = application:get_env(barrel_rocksdb_storage, db_open_retry_delay, 2000),
          _ = lager:debug(
            "~s: barrel rocksdb backend retrying ~p in ~p ms after error ~s\n",
            [?MODULE, Path, SleepFor, OpenErr]
          ),
          timer:sleep(SleepFor),
          open_db(Path, Options, Retries - 1, Reason);
        false ->
          {error, Reason}
      end;
    {error, _} = Error ->
      Error
  end.

db_options(#{ <<"in_memory">> := true }) ->
  [{env, memenv} | default_rocksdb_options()];
db_options(_) ->
  Cache = barrel_rdb_cache:get_cache(),
  BlockOptions = [{block_cache, Cache}],
  [{block_based_table_options, BlockOptions} | default_rocksdb_options()].

default_rocksdb_options() ->
  WriteBufferSize = 64 * 1024 * 1024, %% 64MB
  [
    {write_buffer_size, WriteBufferSize}, %% 64MB
    {allow_concurrent_memtable_write, true},
    {enable_write_thread_adaptive_yield, true}
  ].

ensure_started() ->
  {ok, _} = application:ensure_all_started(barrel_rocksdb_storage),
  ok.