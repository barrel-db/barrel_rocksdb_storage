%%%-------------------------------------------------------------------
%% @doc barrel_rocksdb_storage top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(barrel_rocksdb_storage_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
  Specs = [
    %% cache
    #{id => cache,
      start => {barrel_rdb_cache, start_link, []},
      restart => permanent,
      shutdown => infinity,
      type => worker,
      modules => [barrel_rdb_cache]
    }
  ],
  {ok, { {one_for_one, 4, 3600}, Specs} }.

%%====================================================================
%% Internal functions
%%====================================================================
